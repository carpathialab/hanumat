<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
/******
 * Gestión intermedia de seguridad. Este es nuestro guardían de la puerta.
 * 
 */
require_once('interno/defs.php');
require_once('interno/funciones.php');
if (isset($_SERVER["HTTP_USER_AGENT"]) && strpos($_SERVER["HTTP_USER_AGENT"], "Android") !== false) {
	//Aquí validamos que exista la variable de usuario, como override se puede establecer $hayUsuario = TRUE y luego poner valores arbitrarios.
	//q = identificador del dispositivo, r sigue siendo la petición
	if (isset($_POST['q']) && isset($_POST['r'])) {
		//recordad, niños: r es la página de retorno, pero nos sirve también para saber que es una consulta y un montón de cosas más.
		header('Content-Type: text/html; charset=utf-8');
		$movilCreds = explode("|", $_POST['q'], 2);
		error_log("Credenciales del móvil: ".print_r($movilCreds, true));
		//Los tres estados de una aplicación:
		//Cuando la instalaron y está registrándose como dispositivo.
		//Cuando hay una cuenta de usuario registrado activa...
		//Cuando se desfirma pero sigue instalado
		//Cuando se desinstala???
		switch ($_POST['r']) {
			case 'n':   //Nuevo dispositivo en el barrio...
			case 's':
			case 'z':
			case 'l':   //Firmados e inicios de sesión
				error_log("Es un registro, inicio de sesión, etc...");
				break;
			default:
				//Construimos el token...
				openssl_public_encrypt($_POST['r'] . '|' . DOMINIO . '|' . $movilCreds[1], $token, $_POST['qq']);
				$_POST['token'] = $token;
				$_POST['idU'] = $movilCreds[1];
				$_POST = preparaConsulta($_POST);
				break;
		}
		
		$serv = DOMINIO . str_replace('hanumApp.php', 'interno', $_SERVER['REQUEST_URI']);
		$url = $serv;
		$res = "";
		switch ($_POST['r']) {
			case 'n':   //Nuevo dispositivo en el barrio...
				$_POST['token'] = ofusca("elchiconuevodelbarrio");
				$_POST['idD'] = $movilCreds[0];
				$_POST['devN'] = $movilCreds[1];
				$url .= '/nuevousrapp.php';
				break;
			case 'e':   //Seleccionar registros...
				$url .= '/doSelect.php';
				break;
			case 'l':   //Firmados e inicios de sesión
				$_POST['token'] = ofusca("hanumappelpoderoso!");
				if ($movilCreds[1] == 't')
					if (isset($_POST['f'])) {
						$url .= '/' . $_POST['f'];
					} else {
						$_POST['q'] = $movilCreds[0];
						$url .= '/checkloginapp.php';
					}
					
				else if($movilCreds[1] == 'n')
					$url .= '/nuevousrpublicoapp.php';
				break;
			case 'a':   //Autocompletar
				$url .= '/autocompletar.php';
				break;
			case 'b':
				$url .= '/dobusqueda.php';
				break;
			case 'c':   //Realmente será necesario?
				$url .= '/dolista.php';
				break;
			case 'x':   //Ejecuta un script;
				$url .= '/' . esclarece($_POST['f']);
				break;
			case 's':
				$_POST['idU'] = $movilCreds[1];
				//session_destroy();
				$url .= '/logout.php';
				break;
			case 'f':   //Archivo adjunto...
				//Los entredichos del archivo:
				//Se guarda en una carpeta (puede ser una carpeta compartida por NFS o algo así)
				$url = "";
				if (isset($_FILES)) {
					$res = guardaBinario($_FILES, (isset($_POST['cp']) && $_POST['cp'] == '1' ? TRUE : FALSE), (isset($_POST['re']) ? $_POST['re'] : 0), (isset($_POST['sm']) && $_POST['sm'] == '1' ? TRUE : FALSE));
				} else {
					//registrar el error
					$res = '{"error":"33", "errmsg":"Petición incoherente"}';
				}
				break;
			case 't':	//Se trae la estructura de una tabla...
				$url .= '/doEstructura.php';
				break;
			default:
				if (isset($_FILES)) {
					$res = guardaBinario($_FILES, (isset($_POST['cp']) && $_POST['cp'] == '1' ? TRUE : FALSE), (isset($_POST['re']) ? $_POST['re'] : 0), (isset($_POST['sm']) && $_POST['sm'] == '1' ? TRUE : FALSE));
					$json = json_decode($res);
					error_log("Objeto JSON: " . print_r($json, true));
					if (is_object($json) && $json->error != "0") {
						error_log("Encuentra un error cargando el archivo: " + $json->errmsg);
					} else {
						for ($i = 0; $i < count($json); $i++) {
							$o = $json[$i];
							if ($o->error == "0") {
								$_POST[$o->nombre] = $o->archivo;
							}
						}
					}
				}
				$url .= '/doDB.php';
				break;
		}
		if ($url != "") {
			$opts = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($_POST)
				),
				'ssl' => array(
					"verify_peer"=>false,
					"verify_peer_name"=>false
				)
			);
			$contexto  = stream_context_create($opts);
			$res = file_get_contents($url, false, $contexto);
			//error_log($url);
		}
		if ($res !== FALSE) {
			$json = json_decode($res);
			if ($json->error == '0') {
				//Tenemos un query exitoso
				echo($res);
			} else if ($json->error == '21') {
				//Error en el engine de datos
				registraError("Error interno: {$json->error} - {$json->errmsg}", 10);
				echo('{"error":"1", "errmsg":"No hay información disponible"}');
			} else {
				registraError("Error interno: {$json->error} - {$json->errmsg}", 10);
				error_log("Datos: ".print_r($json, TRUE));
				echo('{"error":"'.$json->error.'", "errmsg":"'.$json->errmsg.'"}');
			}
		} else {
			registraError("FALLO DE CONEXIÓN", 70);
		}
	} else {
		//Error validando el query
		error_log(print_r($_POST, true));
		registraError("CONSULTA INVÁLIDA", 90);
	}
} else {
	//Le falta barrio
	registraError("FUERA DE DOMINIO", 100);
}

function registraError($tipo, $severidad) {
	//Volcamos el error al archivo en disco, o lo enviamos al manejador de errores
	//TODO Aquí va la línea del manejador de errores y firewall
	//A mayor severidad, más problemas
	error_log("HanumApp haciendo su trabajo: $tipo, $severidad");
}

function guardaBinario($arrArchivos, $conCoordenadas = false, $redimensiona = 0, $quitaMetadatos = true) {
	//Necesitamos saber los tipos válidos de archivos...
	$TIPOS_IMAGEN = array("image/jpeg", "image/gif", "image/png");
	$TIPOS_DOCUMENTO = array("application/pdf");
	error_log("Entrando a cargar archivo: " . print_r($arrArchivos, true));
	$arrValidos = array_merge($TIPOS_IMAGEN, $TIPOS_DOCUMENTO);
	$retval = '[';
	foreach ($arrArchivos as $k => $archivo) {
		if (in_array($archivo["type"], $arrValidos) && $archivo["error"] == 0) {
			//Todo bien...
			$dest = SUBIDAS . md5(uniqid().time());
			if (move_uploaded_file($archivo["tmp_name"], $dest)) {
				//preprocesamientos...
				if (in_array($archivo["type"], $TIPOS_IMAGEN)) procesaImagen($dest, $conCoordenadas, $redimensiona, $quitaMetadatos);
				//TODO en caso necesario, preprocesamos otros documentos
				$retval .= '{"archivo":"cache||'.$dest.'","mime":"'.$archivo["type"].'","error":"0","nombre":"'.ofusca(str_replace("(", "[", str_replace(")", "]", substr($k, 1, -1)))).'"},';
			} else {
				$retval = '{"error":"31", "errmsg":"No pudo moverlo"}';
			}
		} else {
			$retval = '{"error":"32", "errmsg":"Tipo de archivo inválido"}';
		}
	}
	if (strpos($retval, "[") !== FALSE) $retval = substr($retval, 0, -1) . ']';
	return $retval;
}
/*
function procesaArchivo($archivo, $procesador = null) {
	$retval = "";
	if ($procesador != null) {
		$res = shell_exec("$procesador \"$archivo\"");
		$retval = '"registros":'.$res;
	}
	return $retval;
}
 * 
 */
function procesaImagen($dest, $conCoordenadas = false, $redimensiona = 0, $quitaMetadatos = true) {
	$laImg = new Imagick($dest);
	$retval = "";
	if ($redimensiona != 0) {
		$laImg->resizeimage($redimensiona, $redimensiona, Imagick::FILTER_POINT, 0.9, true);
	}
	if (isset($arrExif['Orientation'])) {
		//Vamos a ver qué pedo con la orientación...
		switch($arrExif['Orientation']) {
			case 3:
				$laImg->rotateimage(new ImagickPixel(), 180);
				break;
			case 6:
				$laImg->rotateimage(new ImagickPixel(), 90);
				break;
			case 8:
				$laImg->rotateimage(new ImagickPixel(), -90);
				break;
		}
	}
	if ($conCoordenadas) {
		$arrExif = exif_read_data($tmp);
		if (isset($arrExif['GPSLatitude'])) {
			$lat = gps($arrExif["GPSLatitude"], $arrExif['GPSLatitudeRef']);
			$long = gps($arrExif["GPSLongitude"], $arrExif['GPSLongitudeRef']);
			$retval = '"longitud":"'.$long.'","latitud":"'.$lat.'",';
		}
	}
	if ($quitaMetadatos) $laImg->stripimage();
	$laImg->writeimage($dest);
	$laImg->clear();
	$laImg->destroy();
	return $retval;
}

function preparaConsulta($peticion) {
	$retval = array();
	foreach ($peticion as $llave => $valor) {
		$paso = "";
		if ($llave == "c" && strpos($valor, ",") !== FALSE) {
			$prep = explode(",", $valor);
			foreach ($prep as $pp) {
				$paso .= substr(procParte($pp), 0, -1) . ',';
				error_log("PARTES: " . $paso);
			}
		} else {
			$paso .= procParte($valor);
		}
		error_log("Procesando: $llave");
		if (substr($llave, 0, 1) == "<" && substr($llave, -1, 1) == ">") {
			$llave = ofusca(str_replace("(", "[", str_replace(")", "]", substr($llave, 1, -1))));
		}
		$retval[$llave] = substr($paso, 0, -1);
	}
	error_log("Consulta preparada: " . print_r($retval, true));
	return $retval;
}

function procParte($valor) {
	$partes = explode("|", $valor);
	$paso = "";
	foreach($partes as $p) {
		if (substr($p, 0, 1) == "<" && substr($p, -1, 1) == ">") {
			$p = ofusca(substr($p, 1, -1));
		}
		$paso .= "$p|";
	}
	return $paso;
}
?>