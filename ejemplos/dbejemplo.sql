/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
*******************************************************************
* Este archivo es un ejemplo de la base de datos default para Hanumat.
* Es importante reemplazar la cadena: CAMBIA_MI_NOMBRE por el nombre de la base de datos a usar.
*/
create database CAMBIA_MI_NOMBRE charset utf8;
use CAMBIA_MI_NOMBRE;
grant select, insert, update, execute on CAMBIA_MI_NOMBRE.* to usrCAMBIA_MI_NOMBRE@'localhost' identified by 'CAMBIA_EL_PASSWORD';
CREATE TABLE virt_usuario (idusuario int(11) NOT NULL, nombrecompleto varchar(200) NOT NULL,rol varchar(20) DEFAULT NULL,permisos varchar(2000) DEFAULT NULL,email varchar(140) NOT NULL,passwd varchar(41) NOT NULL,privada varchar(700) DEFAULT NULL,ultimo_login timestamp NULL DEFAULT NULL,pagina_inicial varchar(45) DEFAULT NULL,PRIMARY KEY (idusuario),UNIQUE KEY email_UNIQUE (email)) ENGINE=MEMORY DEFAULT CHARSET=utf8;
CREATE TABLE usuario (id int(11) NOT NULL AUTO_INCREMENT,email varchar(150) NOT NULL COMMENT 'Correo electrónico:',passwd varchar(41) NOT NULL,nombrecompleto varchar(200) NOT NULL COMMENT 'Nombre del usuario',idrol int(11) DEFAULT NULL COMMENT 'Rol de usuario:|C rol,id,nombre',momento_alta timestamp NULL DEFAULT CURRENT_TIMESTAMP,activo char(1) NOT NULL DEFAULT '1' COMMENT 'Activo:',PRIMARY KEY (id),UNIQUE KEY email_UNIQUE (email)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE rol (id int(11) NOT NULL AUTO_INCREMENT,nombre varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Nombre del rol',pagina_inicial varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '/inicia.php' COMMENT 'Página donde inicia',activo char(1) CHARACTER SET utf8 DEFAULT '1' COMMENT 'Activo',PRIMARY KEY (id),UNIQUE KEY nombre_UNIQUE (nombre)) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE pagina_rol (idrol int(11) NOT NULL,pagina varchar(200) CHARACTER SET utf8 NOT NULL COMMENT 'Página:|T select',leer char(1) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT 'Leer',cambiar char(1) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT 'Cambiar',borrar char(1) CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT 'Borrar',PRIMARY KEY (idrol,pagina)) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE cliente (id int(11) NOT NULL AUTO_INCREMENT,nombre varchar(100) NOT NULL COMMENT 'Nombre del cliente:',contacto varchar(150) DEFAULT NULL COMMENT 'Contacto principal:',fecha_alta timestamp NULL DEFAULT CURRENT_TIMESTAMP,foto_principal blob COMMENT 'Fotografía', foto_principal_mime varchar(45),PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
delimiter @@
CREATE FUNCTION dameDerechos(elRol integer) RETURNS varchar(300) CHARSET utf8
	READS SQL DATA
BEGIN
declare retval varchar(300) default '';
select group_concat(concat(pagina,'[]=',concat_ws(concat('&',pagina, '[]='), leer, cambiar, borrar)) separator '&') into retval from pagina_rol where idrol = elRol group by idrol;
RETURN retval;
END@@
CREATE PROCEDURE actualizaAccesos(elID integer)
	READS SQL DATA
BEGIN
	declare nomusr varchar(150) default '';
	declare losRoles varchar(150) default '';
	declare losPermisos varchar(500) default '';
	declare elPwd char(41);
	declare elCorreo varchar(120);
	declare laPag varchar(400) default '';
	select nombrecompleto, idrol, passwd, email into nomusr, losRoles, elPwd, elCorreo from usuario where id = elID;
	select rol.pagina_inicial, dameDerechos(usuario.idrol)
	into laPag, losPermisos
	from usuario 
	inner join rol on usuario.idrol = rol.id
	where usuario.id = elID group by usuario.id;
	insert into virt_usuario(idusuario, nombrecompleto, rol, permisos, email, passwd, pagina_inicial) values (elID, nomusr, losRoles, losPermisos, elCorreo, elPwd, laPag);
END@@
CREATE TRIGGER usuario_AFTER_INSERT AFTER INSERT ON usuario FOR EACH ROW
BEGIN
	if new.activo = 1 then
		call actualizaAccesos(new.id);
	end if;
END@@
CREATE TRIGGER suario_BEFORE_UPDATE BEFORE UPDATE ON usuario FOR EACH ROW
BEGIN
	if new.passwd != old.passwd then
		update virt_usuario set passwd = new.passwd where idusuario = new.id;
	end if;
	if new.activo = '0' then
		delete from virt_usuario where idusuario = old.id;
	elseif new.activo = '1' and old.activo != new.activo then
		call actualizaAccesos(new.id);
	end if;
END@@
CREATE TRIGGER pagina_rol_BEFORE_INSERT BEFORE INSERT ON pagina_rol FOR EACH ROW
BEGIN
	declare ultID integer default -1;
	if (new.idrol = -1) then
		select max(id) into ultID from rol;
		set new.idrol = ultID;
	end if;
END@@
delimiter ;
insert into rol values (null, 'Rol de pruebas', 'inicio.php', '1');
insert into pagina_rol values ('1', 'inicio.php', '1', '1', '1');
insert into pagina_rol values ('1', 'clientes.php', '1', '1', '1');
insert into pagina_rol values ('1', 'usuarios.php', '1', '1', '1');
insert into pagina_rol values ('1', 'roles.php', '1', '1', '1');
create view vc_cliente as select id as id, foto_principal as Foto, foto_principal_mime as Foto_mime, nombre as Nombre, contacto as Contacto, date_format(fecha_alta, '%H:%i %d/%m/%Y') as Alta from cliente;
create view vc_usuario as select usuario.id as id, nombrecompleto as Nombre, email as Correo, rol.nombre as Rol from usuario inner join rol on usuario.idrol = rol.id where usuario.activo = '1';
create view vc_rol as select rol.id as id, nombre as Nombre, pagina_inicial as Inicia, group_concat(pagina_rol.pagina separator '<br>') as Pags from rol inner join pagina_rol on rol.id = pagina_rol.idrol where rol.activo = '1' group by rol.id;
