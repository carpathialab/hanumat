<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
require_once('defs.php');
require_once('conexion.php');
require_once('funciones.php');
$retval = "";
if (isset($_POST['token'])) {
	header('Content-Type: text/html; charset=utf-8');
	if ($dbcon = conectaDB()) {
		if (validaToken($_POST['token'], $dbcon, $_POST['r'], $_POST['idU'])) {
			$tabla = esclarece($_POST["t"]);
			$qry="select " . esclarece($_POST["l"]) . " from $tabla where " . resuelveCond($_POST["c"]) . ";";
			//error_log("Consulta: $qry");
			$result = $dbcon->query($qry);
			//Tenemos una sola fila...
			if ($result->num_rows == 1) {
				$fila = $result->fetch_array();
				$retval = '';
				//Vamos a crear el JSON...
				$cols = $result->fetch_fields();
				$arrRet = array("error" => "0");
				//$retval = '{"error":"0",';
				foreach ($cols as $col) {
					//error_log("Datos del campo {$col->name}: {$col->type} : {$col->flags}");
					if ($col->type == 252 && $col->flags & 128) {
						//FIXIT traer / identificar el MIME para enivarlo...
						$mime = $fila[$col->name . "_mime"];
						//error_log("La imagen: " . print_r($fila[$col->name], TRUE));
						$arrRet[ofusca($tabla . '_' . $col->name)] = 'data:' . $mime . ';base64,'.base64_encode($fila[$col->name]);
						//$retval .= '"' . ofusca($tabla . '_' . $col->name).'": "data:' . $mime . ';base64,'.base64_encode($fila[$col->name]).'",';
						//error_log("Cuando pone la imagen: $retval");
					} else {
						$arrRet[ofusca($tabla . '_' . $col->name)] = $fila[$col->name];
						//$retval .= '"' . ofusca($tabla . '_' . $col->name).'": "'.$fila[$col->name].'",';
					}
				}
				$retval = json_encode($arrRet);
				//$retval = substr($retval, 0, -1) . "}";
			} else {
				error_log("Consulta fallida al seleccionar: $qry");
				$retval = '{"error":"21", "errmsg":"No hay datos"}';
			}
		} else {
			//Error: token o sesión inválida
			$retval = '{"error":"14", "errmsg":"Token inválido"}';
		}
	} else {
		//Error con la base de datos
		$retval = '{"error":"12", "errmsg":"Problemas de base de datos"}';
	}
} else {
	//Petición incorrecta
	$retval = '{"error":"11", "errmsg":"Sesión inválida"}';
}
echo $retval;
?>
