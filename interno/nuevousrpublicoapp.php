<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
//Sirve para dar de alta usuarios desde el autofirmado...
require_once("./defs.php");
require_once("./funciones.php");
require_once("./conexion.php");
if (isset($_POST['token']) && esclarece($_POST['token']) == "hanumappelpoderoso!") {
	//antes de seguir validamos que tenga datos en todo...
	$pasa = TRUE;
	foreach($_POST as $val) {
		$pasa = $val == "" ? $pasa && false : $pasa && true;
	}
	if ($pasa) {
		$dbcon = conectaDB();
		//Tal como está configurado, saca la sesión de otro lugar una vez que ingrese.
		$consulta = $dbcon->prepare("SELECT usuarioAutoservicio(?, ?, ?, ?, ?);");
		$consulta->bind_param("sssss", $_POST['nombre'], $_POST['apellidos'], $_POST['nivel'], $_POST['correo'], $_POST['contras']);
		$consulta->execute();
		$consulta->bind_result($ret);
		$retval = '{"error":"1", "errmsg":"No se pudo registrar"}';
		//error_log("Entrando a validar...");
		while ($consulta->fetch()) {
			//Generar la llave pública y privada, y guardar la sesión en la base de datos
			//error_log("respuesta: $ret");
			if ($ret != 0)
				$retval = '{"error":"0", "idnuevo":"'.$ret.'"}';
		}
		$consulta->close();
		//$rs->free();
		echo $retval;
	} else {
		header('HTTP/1.0 404 Not Found', true, 404);
		//var_dump($_POST);
		die();
	}
} else {
	header('HTTP/1.0 404 Not Found', true, 404);
	//var_dump($_POST);
	die();
}
?>