<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
//require_once('interno/defs.php');
function esMovil() {
	return preg_match("/(kalli_app|android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

function ofusca($texto) {
    $datos = gzcompress($texto, 9);
    $retval = base64_encode($datos);
    return $retval;
}

function esclarece($datos) {
    $dat = base64_decode($datos);
    $retval = gzuncompress($dat);
    return $retval;
}

function resuelveCond($datos) {
    $arrMult = explode(",", $datos);
    $retval = "";
    foreach($arrMult as $parte) {
        if ($parte != "") {
            $arrCond = explode("|", $parte, 2);
            $campo = esclarece($arrCond[0]);
            if (substr($campo, 0, 1) == '*') {
                $retval = "`". substr($campo, 1) . "` like ('%{$arrCond[1]}%') and ";
            } else if (substr($campo, 0, 1) == '-') {
                $cr = explode("><", $arrCond[1], 2);
                $retval = "(`" . substr($campo, 1) . "` >= '{$cr[0]}' and `" . substr($campo, 1) . "` <= '{$cr[1]}') and ";
            } else {
                $retval .= esclarece($arrCond[0]) . " in ('{$arrCond[1]}') and ";
            }
        }
    }
    //error_log("Condición obtenida: $retval");
    $retval = substr($retval, 0, -5);
    return $retval;
}

function validaToken($token, $dbcon, $r, $idusr) {
    //Validamos el token de sesión antes de seguir adelante
    //TODO validar que exista la sesión en la base de datos.
    //$_POST['r'] . '|' . DOMINIO . '|' . $_SESSION['Usuario']['id']
    //Traemos la llave privada de la DB
    $retval = FALSE;
    $qry = "SELECT privada FROM virt_usuario WHERE idusuario = '{$idusr}';";
    //error_log("Intentando token estricto");
    $rs = $dbcon->query($qry);
    if ($rs) {
        $fila = $rs->fetch_row();
		//error_log($token);
        openssl_private_decrypt($token, $res, $fila[0]);
        //error_log("Resultado después de desencriptar el token: $res");
        $retval = ($res == $r . '|' . DOMINIO . '|' . $idusr);
    } else {
        //error_log("Base de datos dijo: " . $dbcon->error);
    }
    return $retval;
}

function dameCuadrumano() {
    //Devuelve un string con el siguiente cuadrumano disponible.
    if (count($ejercitoCuadrumano) < 2) {
        return $ejercitoCuadrumano[0];
    } else {
        //TODO random seed para número entre 0 y cuadrumanos - 1, devolvemos ese índice...
        
    }
}

function fonetico($cadena) {
	$FONETICO = array('P'=>'0','B'=>'1','V'=>'1','F'=>'2','H'=>'2','T'=>'3','D'=>'3','S'=>'4','Z'=>'4','C'=>'4','X'=>'4','Y'=>'5','L'=>'5','N'=>'6','Ñ'=>'6','M'=>'6','Q'=>'7','K'=>'7','G'=>'8','J'=>'8','R'=>'9');
	$retval = "";
	$paso = "";
	$ltr = "";
	$cadena = str_replace('LL', 'L', strtoupper($cadena));
	$cadena = str_replace('RR', 'R', $cadena);
	if (strpos($cadena, 'H')) $cadena = substr($cadena, 1);
	if (substr($cadena, -1, 1) == 'S' || substr($cadena, -1, 1) == 'N') $cadena = substr($cadena, 0, -1);
	
	for ($i = 0; $i < strlen($cadena); $i++) {
		$ltr = substr($cadena, $i, 1);
		switch ($ltr) {
			case 'A': case 'E': case 'I': case 'O': case 'U': case 'W': case '.': case ',': case ';': case ':': case '"': case 'Á': case 'É': case 'Í': case 'Ó': case 'Ú': case 'Ü':
				$paso = '';
				break;
			default:
				$paso = $FONETICO[$ltr];
				$retval .= $paso;
				break;
		}
	}
	return $retval;
}
?>